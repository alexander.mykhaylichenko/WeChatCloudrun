const path = require("path");
const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const dotenv = require("dotenv");
const { XMLParser, XMLValidator } = require("fast-xml-parser");
const axios = require("axios");
const bodyParser = require('body-parser');
//const { init: initDB, Counter } = require("./db");

const logger = morgan("tiny");
dotenv.config();

const { SECRET_KEY, APP_ID } = process.env;

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(logger);

// 首页
// app.get("/MP_verify_sjMpsGe2DWEI3Y3N.txt", async (req, res) => {
//   res.sendFile(path.join(__dirname, "MP_verify_sjMpsGe2DWEI3Y3N.txt"));
// });

app.post("/ticket", async (req, res) => {
  console.log('post ticket', req.body);

  res.send(200);
});

app.post('/get', async (req, res) => {
  const token = await axios.get('http://127.0.0.1:8081/inner/component-access-token');
  res.send({
    code: 200,
    data: token.toString()
  });
});

const port = process.env.PORT || 8080;

async function bootstrap() {
  // await initDB();
  app.listen(port, () => {
    console.log("启动成功", port);
  });
}

bootstrap();
